# Setup and overview

Along with the link to this page, you should have received credentials for a temporary Azure user account. The account will have the following limits:

* User has access to only one resource group
  
* The work below and needed resources must be created and managed in that resource group
  
* The resource group has a budget of $10
  
* The user account and access is only guaranteed to be available for 14 days from when you receive the credentials. Please manage the time carefully as you want to make sure that the evaluator can review a submission that has not expended its budget.

## What you'll need
Please use the project in this repository: https://bitbucket.org/hriinc/eval-fast-bk-1-37dk/src/master/ to set up and host a website.

## Requirements

* Hosted website should be publicly accessible

* When going to the website, the default page served by the app in that repository should be displayed (this will simply be an empty page with a large centered Fort logo).

* Website should be served over HTTPS

* Please send us the website url as your submission


## Keep in mind

* The project we've provided should not need any code changes. The task is to get that project to run and host it. There should be enough information in the project and google to figure out how to run it if you're unfamiliar with the stack.

* If you run into any issues, or are not able to arrive at a working solution, just attach an explanation with your submission detailing what you tried, what worked, what didn't, and any other information you feel is useful.

* If you have any questions, please reach out to your point of contact and we'll respond as soon as we are able.
